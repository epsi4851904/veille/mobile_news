import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final storage = FlutterSecureStorage();

  Future<void> _login() async {
    if (_formKey.currentState?.validate() ?? false) {
      final url = Uri.parse('http://localhost:3000/user/login');
      final response = await http.post(
        url,
        body: {
          'email': _emailController.text,
          'password': _passwordController.text,
        },
      );
      if (response.statusCode == 201) {
        final jwtWithBearer = response.body;
        final jwt = jwtWithBearer.split('"')[3];
        await storage.write(key: 'jwt', value: 'Bearer $jwt');
        Navigator.pushNamed(context, '/home');
      } else {
        print(response.body);
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Login failed')),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     resizeToAvoidBottomInset: true,
     body: Container(
       height: double.infinity,
       width: double.infinity,
       decoration: const BoxDecoration(
       color: Color.fromARGB(255, 18, 33, 55)
       ),
       child: Column(
         children: [
          const SizedBox(
             height: 60,
          ),
          Row(
            children: [
              ElevatedButton(
                child: Icon(Icons.arrow_back, color: Colors.white, size: 30, ),
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/welcome');
                }, 
              )
            ]
          ),
          const SizedBox(
             height: 60,
          ),
           const Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
              
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                Text(
                  'Hello',
                  style: TextStyle(
                  fontSize: 40,
                  color: Colors.white,
                  ),
                ),
                SizedBox(width: 20),
                Icon(Icons.waving_hand, color: Colors.white, size: 30,),
                ],
              ),
              
              SizedBox(
                height: 20,
              ),
              Text(
                'Vous revoilà !!\nNous sommes ravis de vous revoir',
                style: TextStyle(
                fontSize: 20,
                color: Color.fromARGB(255, 153, 153, 153),
                ),
              ),
              ],
            ),
            ),
          SingleChildScrollView(
              scrollDirection: Axis.vertical,
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 80),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        style: const TextStyle(
                            color: Colors.white,
                        ),
                        decoration:  const InputDecoration(
                          labelText: 'Email',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromARGB(255, 197, 197, 197)
                          ),
                          prefixIcon: Icon(Icons.email, color: Colors.white),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),

                        
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter your email';
                          }
                          if (!RegExp(r'^[^@]+@[^@]+\.[^@]+').hasMatch(value)) {
                            return 'Please enter a valid email address';
                          }
                          return null;
                        },
                      ),
                    const SizedBox(height: 20),
                    TextFormField(
                      controller: _passwordController,
                      obscureText: true,
                      style: const TextStyle(
                            color: Colors.white,
                        ),
                      decoration: const InputDecoration(
                        labelText: 'Password',
                        labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 197, 197, 197),
                        ),
                        prefixIcon: Icon(Icons.lock, color: Colors.white),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your password';
                        }
                        if (value.length < 6) {
                          return 'Password must be at least 6 characters long';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 50),
                    ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState?.validate() ?? false) {
                          // Perform login action
                          _login();
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(horizontal: 120, vertical: 15),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        backgroundColor: Colors.white,
                      ),
                      child: const Text(
                        'Login',
                        style: TextStyle(fontSize: 18, color: Colors.black),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          'Don\'t have an account?',
                          style: TextStyle(
                            fontSize: 15,
                            color: Color.fromARGB(255, 153, 153, 153),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/register');
                          },
                          child: const Text(
                            'Sign up',
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(  
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 1.0,
                          width: 100,
                          color: Colors.white,
                        ),
                        const SizedBox(width: 10),
                        const Text(
                          'Ou',
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 10),
                        Container(
                          height: 1.0,
                          width: 100,
                          color: Colors.white,
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    const Row(  
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image(image: AssetImage('assets/google_logo.png'), height: 75, width: 75),
                        SizedBox(width: 50),
                        Image(image: AssetImage('assets/facebook_logo.png'), height: 75, width: 75),
                      ],
                    ),
                    
                ],
              ),
            ),
          //  const Image(image: AssetImage('assets/social.png'))
       )]
       ),
     ),

    );
  }
}