import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:math';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreen createState() => _RegisterScreen();
}

class _RegisterScreen extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool platformResponse = false;
  String code = '';
  

  String generateCode() {
    const chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    final random = Random();
    final codeLength = 6;

    for (int i = 0; i < codeLength; i++) {
      final randomIndex = random.nextInt(chars.length);
      code += chars[randomIndex];
    }

    print(code);

    return code;
  }

  void sendEmailVerification(String emailToSend, String password) async {
    
    print('Email: $emailToSend, Password: $password');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     resizeToAvoidBottomInset: true,
     body: Container(
       height: double.infinity,
       width: double.infinity,
       decoration: const BoxDecoration(
       color: Color.fromARGB(255, 18, 33, 55)
       ),
       child: Column(
         children: [
          const SizedBox(
             height: 60,
          ),
          Row(
            children: [
              ElevatedButton(
                child: Icon(Icons.arrow_back, color: Colors.white, size: 30, ),
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/welcome');
                }, 
              )
            ]
          ),
          const SizedBox(
             height: 30,
          ),
           const Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
              
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                Text(
                  'Bienvenue !',
                  style: TextStyle(
                  fontSize: 40,
                  color: Colors.white,
                  ),
                ),
                SizedBox(width: 20),
                Icon(Icons.waving_hand, color: Colors.white, size: 30,),
                ],
              ),
              
              SizedBox(
                height: 20,
              ),
              Text(
                'Créer vous un compte !!\nPour avoir accés aux dèrnieres news',
                style: TextStyle(
                fontSize: 20,
                color: Color.fromARGB(255, 153, 153, 153),
                ),
              ),
              ],
            ),
            ),
          SingleChildScrollView(
              scrollDirection: Axis.vertical,
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 40),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                        controller: _nameController,
                        keyboardType: TextInputType.emailAddress,
                        style: const TextStyle(
                            color: Colors.white,
                        ),
                        decoration:  const InputDecoration(
                          labelText: 'Nom d\'utilisateur',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromARGB(255, 197, 197, 197)
                          ),
                          prefixIcon: Icon(Icons.badge, color: Colors.white),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),

                        
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Entrer un nom d\'utilisateur';
                          }
                          return null;
                        },
                      ),
                    const SizedBox(height: 20),
                    TextFormField(
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        style: const TextStyle(
                            color: Colors.white,
                        ),
                        decoration:  const InputDecoration(
                          labelText: 'Email',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromARGB(255, 197, 197, 197)
                          ),
                          prefixIcon: Icon(Icons.email, color: Colors.white),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                          ),

                        
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Entrer votre email';
                          }
                          if (!RegExp(r'^[^@]+@[^@]+\.[^@]+').hasMatch(value)) {
                            return 'Entrer un email valide';
                          }
                          return null;
                        },
                      ),
                    const SizedBox(height: 20),
                    TextFormField(
                      controller: _passwordController,
                      obscureText: true,
                      style: const TextStyle(
                            color: Colors.white,
                        ),
                      decoration: const InputDecoration(
                        labelText: 'Mot de passe',
                        labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 197, 197, 197),
                        ),
                        prefixIcon: Icon(Icons.lock, color: Colors.white),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Entre votre mot de passe';
                        }
                        if (value.length < 6) {
                          return 'Le mot de passe doit contenir au moins 6 caractères';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 50),
                    ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState?.validate() ?? false) {
                          // Perform login action
                          sendEmailVerification(_emailController.text, _passwordController.text);
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Création du compte...')),
                          );
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(horizontal: 60, vertical: 15),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        backgroundColor: Colors.white,
                      ),
                      child: const Text(
                        'Créer un compte',
                        style: TextStyle(fontSize: 18, color: Colors.black),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          'Vous avez déjà un compte ?',
                          style: TextStyle(
                            fontSize: 15,
                            color: Color.fromARGB(255, 153, 153, 153),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: const Text(
                            'Connectez-vous',
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(  
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 1.0,
                          width: 100,
                          color: Colors.white,
                        ),
                        const SizedBox(width: 10),
                        const Text(
                          'Ou',
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 10),
                        Container(
                          height: 1.0,
                          width: 100,
                          color: Colors.white,
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    const Row(  
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image(image: AssetImage('assets/google_logo.png'), height: 75, width: 75),
                        SizedBox(width: 50),
                        Image(image: AssetImage('assets/facebook_logo.png'), height: 75, width: 75),
                      ],
                    ),
                    
                ],
              ),
            ),
       )]
       ),
     ),

    );
  }
}