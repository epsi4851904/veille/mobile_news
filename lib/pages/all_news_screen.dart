import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:sticky_headers/sticky_headers.dart';
import 'package:veille_app/components/news_components.dart';
import 'package:veille_app/components/news_components_top.dart';


class AllNewsWorldScreen extends StatefulWidget {
  @override
  _AllNewsWorldScreenState createState() => _AllNewsWorldScreenState();
}

class _AllNewsWorldScreenState extends State<AllNewsWorldScreen> {
   final storage = const FlutterSecureStorage();
   var news = [];
   var flux = [];

  @override
  void initState() {
    super.initState();
    _getNews();
  }


  Future<void> _getNews() async {
    final jwt = await storage.read(key: 'jwt');
    final url = Uri.parse('http://localhost:3000/news');
    print(jwt!);
    final response = await http.get(
      url, 
      headers: {
      'Authorization': jwt,
    });
    if (response.statusCode == 200) {
      news = jsonDecode(response.body);
      print(news[1]);
      setState(() {
        news = news;
      });
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
          SizedBox(height: 20,),
          Padding(padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10,),
            child: Text('Top News', style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                )),
          ),
          Container(
              height: 300,
              child: ListView.builder(
                itemCount: news.length,
                itemBuilder: (context, index) => NewsComponentTop(
                  title: news[index]['title'], 
                  description: news[index]['description'], 
                  imageUrl: news[index]['pictureURL'],
                  source: news[index]['sourceURL'], 
                  views: 0,
                  text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 
                ),
                scrollDirection: Axis.horizontal,

              ),
              ),
          const SizedBox(
             height: 20,
          ),
          StickyHeader( 
            header: Container(
              height: 60.0,
              color: Colors.white,
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              alignment: Alignment.centerLeft,
              child: const Text('Les dérnières news', style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  )),
            ),
            content: Column(
              children: List.generate(10, (index) => Padding(
                padding:  const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: NewsComponent(
                title: 'News Title $index', 
                description: 'Description $index', 
                imageUrl: 'https://example.com/image_$index.jpg', 
                source: 'Source $index', 
                views: 1000 + index, 
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. $index', 
              ),
              )),
            ),
          ),         
      ])
      )
    );
  }
}