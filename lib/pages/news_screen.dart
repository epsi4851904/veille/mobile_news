import 'package:flutter/material.dart';

class NewsScreen extends StatefulWidget {
  final String articleTitle;
  final String articleSource;
  final String articleText;
  final int articleView;
  final String articleImg;

  NewsScreen({
    required this.articleTitle,
    required this.articleImg,
    required this.articleSource,
    required this.articleText,
    required this.articleView,
  });

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  bool _isBottomSheetOpen = false;
  bool _showReview = false;
  bool _isLiked = false;

  var source = '';

  @override
  void initState() {
    super.initState();
    getSourceDomain();
  }
  
 void getSourceDomain() {
  if (widget.articleSource.startsWith('http://') || widget.articleSource.startsWith('https://')) {
    final startIndex = widget.articleSource.indexOf('://') + '://'.length;
    final endIndex = widget.articleSource.indexOf('/', startIndex);
    
    if (endIndex != -1) {
      source = widget.articleSource.substring(startIndex, endIndex);
    } else {
      source = widget.articleSource.substring(startIndex);
    }
    
    print('Original source URL: ${widget.articleSource}');
    print('Extracted domain: $source');
    
    setState(() {
      source = source;
    });
  } else {
    source = 'source inconnue';
  }
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('News Screen'),
        backgroundColor: Colors.transparent,
      ),
      extendBodyBehindAppBar: true,
      body: GestureDetector(
        onVerticalDragEnd: (details) {
          if (details.primaryVelocity! < 0) {
            _toggleBottomSheet();
          } else if (details.primaryVelocity! > 0) {
            _toggleBottomSheet();
          }
        },
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage('https://picsum.photos/2500/1500'),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                bottom: 200,
                left: 16,
                right: 16,
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.5),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  padding: EdgeInsets.all(16),
                  height: 300,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          widget.articleTitle,
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                          softWrap: true,
                          overflow: TextOverflow.clip,
                        ),
                      ),
                      SizedBox(height: 16),
                      Text(
                        widget.articleText,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: 16),
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 16.0,
                left: 0,
                right: 0,
                child: Center(
                  child: Column(
                    children: [
                      Text(
                        'Swipe Up for more',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                      SizedBox(height: 8),
                      Icon(
                        Icons.arrow_upward,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: null,
      bottomSheet: null,
    );
  }

  void _toggleBottomSheet() {
    if (_isBottomSheetOpen) {
      Navigator.of(context).pop();
    } else {
      showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white,
                ),
                padding: EdgeInsets.all(16.0),
                height: 600,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded( // Ensures that the Text widget can wrap to the next line
                          child: Text(
                            widget.articleTitle,
                            style: TextStyle(
                              fontSize: 24.0,
                              fontWeight: FontWeight.bold,
                            ),
                            softWrap: true,
                            overflow: TextOverflow.clip,
                          ),
                        ),
                        IconButton(
                          icon: Icon(
                            _isLiked ? Icons.favorite : Icons.favorite_border,
                            color: _isLiked ? Colors.red : Colors.grey,
                          ),
                          onPressed: () {
                            setState(() {
                              _isLiked = !_isLiked;
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 8.0),
                    Text(
                      source,
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(height: 16.0),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              _showReview = false;
                            });
                          },
                          child: Text(
                            'Information',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight:
                                  _showReview ? FontWeight.normal : FontWeight.bold,
                              color: _showReview ? Colors.grey : Colors.black,
                            ),
                          ),
                        ),
                        SizedBox(width: 16.0),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              _showReview = true;
                            });
                          },
                          child: Text(
                            'Review',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight:
                                  _showReview ? FontWeight.bold : FontWeight.normal,
                              color: _showReview ? Colors.black : Colors.grey,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 16.0),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              _showReview
                                  ? "Afin de nous aider dans notre démarche de présenter des news pertinentes et réelles, vous pouvez nous donner votre point de vue sur cette info."
                                  : widget.articleText,
                              style: TextStyle(fontSize: 16.0),
                            ),
                          ],
                        ),
                      ),
                    ),
                    if (_showReview)
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 16.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                width: 200,
                                child: ElevatedButton(
                                  onPressed: () {
                                    // Action for Real button
                                  },
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.lightBlue[200],
                                    foregroundColor: Colors.white,
                                  ),
                                  child: Center(
                                    child: Text('Réel'),
                                  ),
                                ),
                              ),
                              SizedBox(height: 8.0),
                              Container(
                                width: 200,
                                child: ElevatedButton(
                                  onPressed: () {
                                    // Action for Fake button
                                  },
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.blue,
                                    foregroundColor: Colors.white,
                                  ),
                                  child: Center(
                                    child: Text('Fake'),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              );
            },
          );
        },
      ).whenComplete(() {
        setState(() {
          _isBottomSheetOpen = false;
          _showReview = false;
        });
      });
    }
    setState(() {
      _isBottomSheetOpen = !_isBottomSheetOpen;
    });
  }
}
