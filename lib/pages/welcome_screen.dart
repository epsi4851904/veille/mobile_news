import 'package:flutter/material.dart';
import 'package:veille_app/pages/login_screen.dart';
import 'package:veille_app/pages/register_screen.dart';
import 'package:flutter_svg/flutter_svg.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     body: Container(
       height: double.infinity,
       width: double.infinity,
       decoration: const BoxDecoration(
       color: Color.fromARGB(255, 18, 33, 55)
       ),
       child: Column(
         children: [
          const SizedBox(
             height: 100,
           ),
           SvgPicture.asset('assets/logo1.svg', height: 200, width: 200,),
           const SizedBox(
             height: 100,
           ),
           const Text('Bienvenue !',style: TextStyle(
             fontSize: 30,
             color: Colors.white
           ),),
            const SizedBox(
             height: 20,
           ),
           const Text(
              'Connectez-vous pour avoir accés \n aux dèrnieres news',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15,
                // centerTitle: true,
                color: Color.fromARGB(255, 153, 153, 153),
              ),
            ),
          const SizedBox(height: 30,),
           const SizedBox(height: 30,),
           GestureDetector(
             onTap: (){
               Navigator.push(context,
                   MaterialPageRoute(builder: (context) => const LoginScreen()));
             },
             child: Container(
               height: 53,
               width: 320,
               decoration: BoxDecoration(
                 color: Colors.transparent,
                 borderRadius: BorderRadius.circular(10),
                 border: Border.all(color: Colors.white),
               ),
               child: const Center(child: Text('SE CONNECTER',style: TextStyle(
                   fontSize: 20,
                   fontWeight: FontWeight.bold,
                   color: Colors.white
               ),),),
             ),
           ),
           const SizedBox(height: 30,),
           GestureDetector(
             onTap: (){
               Navigator.push(context,
                   MaterialPageRoute(builder: (context) => const RegisterScreen()));
             },
             child: Container(
               height: 53,
               width: 320,
               decoration: BoxDecoration(
                 color: Colors.white,
                 borderRadius: BorderRadius.circular(10),
                 border: Border.all(color: Colors.white),
               ),
               child: const Center(child: Text('CRÉER UN COMPTE',style: TextStyle(
                   fontSize: 20,
                   fontWeight: FontWeight.bold,
                   color: Colors.black
               ),),),
             ),
           ),
           const Spacer(),
          ]
       ),
     ),

    );
  }
}