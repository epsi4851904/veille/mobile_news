import 'package:flutter/material.dart';

class TopNewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Top News'),
      ),
      body: Center(
        child: Text(
          'This is the top news page.',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}