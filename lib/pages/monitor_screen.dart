import 'package:flutter/material.dart';

class MonitorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Best News Source',
              style: TextStyle(fontSize: 24),
            ),
            SizedBox(height: 16),
            Text(
              'CNN',
              style: TextStyle(fontSize: 48, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 32),
            Text(
              'Fake News Count',
              style: TextStyle(fontSize: 24),
            ),
            SizedBox(height: 16),
            Text(
              '5',
              style: TextStyle(fontSize: 48, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}