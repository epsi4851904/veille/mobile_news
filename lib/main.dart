import 'package:flutter/material.dart';
import 'package:veille_app/pages/home_screen.dart';
import 'package:veille_app/pages/login_screen.dart';
import 'package:veille_app/pages/monitor_screen.dart';
import 'package:veille_app/pages/news_screen.dart';
import 'package:veille_app/pages/profil_screen.dart';
import 'package:veille_app/pages/register_screen.dart';
import 'package:veille_app/pages/top_news_screen.dart';
import 'package:veille_app/pages/welcome_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: WelcomeScreen(),
      routes: {
        '/welcome': (context) => const WelcomeScreen(),
        '/login': (context) => const LoginScreen(),
        '/register': (context) => const RegisterScreen(),
        '/home': (context) => HomeScreen(),
        '/top-news': (context) => TopNewsScreen(),
        '/stat': (context) => MonitorScreen(),
        '/profile': (context) => ProfileScreen(),
      },
    );
  }
}