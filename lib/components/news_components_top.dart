import 'package:flutter/material.dart';
import 'package:veille_app/pages/news_screen.dart';

class NewsComponentTop extends StatelessWidget {
  final String title;
  final String description;
  final String imageUrl;
  final String source;
  final int views;
  final String text;

  const NewsComponentTop({
    required this.title,
    required this.description,
    required this.imageUrl,
    required this.source,
    required this.views,
    required this.text
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => NewsScreen(
              articleTitle: title,
              articleSource: source,
              articleText: text,
              articleView: views,
              articleImg: imageUrl,
            ),
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.all(15),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        width: 200,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3),
            ),
          ],
        ),
        child: Column(
          children: [
            Container(
              height: 100,
              width: 190,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Titre de la news',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'journaldugeek.fr',
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.grey[400],
                  ),
                ),
                const Row(
                  children: [
                    Icon(
                      Icons.visibility_sharp,
                      color: Colors.blue,
                      size: 15,
                    ),
                    SizedBox(width: 5),
                    Text('1.7k'),
                  ],
                )
              ],
            ),
            Container(
              height: 85,
              child: GestureDetector(
                onVerticalDragUpdate: (_) {},
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Text(
                    'lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.grey[700],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ) 
      ),
    );
  }
}